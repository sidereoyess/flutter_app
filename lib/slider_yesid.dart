import 'package:flutter/material.dart';

class SliderExample extends StatefulWidget {
  const SliderExample({Key? key}) : super(key: key);
  //const SliderExample.withKey(Key key) : super(key: key);
  @override
  State<SliderExample> createState() => _SliderExampleState();
}

class _SliderExampleState extends State<SliderExample> {
  double _currentSliderValue = 20;
  var valor = 10;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Slider(
          value: _currentSliderValue,
          max: 100,
          divisions: 50,
          label: _currentSliderValue.round().toString(),
          onChanged: (double value) {
            setState(() {
              _currentSliderValue = value;
              print(value.round().toString());
            });
          },
        ),
        ElevatedButton(
          onPressed: getValue,
          child: Text('Obtener Valor'),
        ),
      ],
    );
  }

  void getValue() {
    print(_currentSliderValue);
  }
}
