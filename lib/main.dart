import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'state_app.dart';
import 'control_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var dark = Brightness.dark;
  IconData icono = Icons.dark_mode;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyAppState(),
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Scale',
          theme: ThemeData(
            brightness: dark,
            useMaterial3: true,
          ),
          home: Scaffold(
            appBar: AppBar(
                title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Bienvenido"),
                IconButton(
                    icon: Icon(icono),
                    onPressed: () {
                      if (dark == Brightness.dark) {
                        dark = Brightness.light;
                        icono = Icons.dark_mode;
                      } else {
                        dark = Brightness.dark;
                        icono = Icons.light_mode;
                      }
                      setState(() {});
                    }),
              ],
            )),
            body: const MyHomePage(),
          )),
    );
  }
}
