import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../slider_yesid.dart';
import '../state_app.dart';
import '../card_text.dart';

class GeneratorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var pair = appState.current;
    SliderExample slider = SliderExample();
    IconData icon;
    if (appState.favorites.contains(pair)) {
      icon = Icons.favorite;
    } else {
      icon = Icons.favorite_border;
    }

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CardText(pair: pair),
          SizedBox(height: 10),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton.icon(
                onPressed: () {
                  appState.toogleFavorite();
                },
                icon: Icon(icon),
                label: Text('Like'),
              ),
              SizedBox(width: 10),
              ElevatedButton(
                onPressed: () {
                  appState.getNext();
                },
                child: Text('Next'),
              ),
              slider,
              ElevatedButton(
                onPressed: () {
                  print(slider.key);
                },
                child: Text('Actualizar valor'),
              )
            ],
          ),
        ],
      ),
    );
  }
}
